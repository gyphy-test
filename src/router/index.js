import Vue from 'vue'
import Router from 'vue-router'
import Search from '@/scenes/Search'
import GifDetail from '@/scenes/GifDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Search',
      component: Search,
      props: (route) => ({ query: route.query.q })
    }, {
      path: '/gifs/show/:id',
      name: 'GifDetail',
      component: GifDetail,
      props: true
    }
  ]
})
